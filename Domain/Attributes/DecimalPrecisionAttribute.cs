﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Attributes
{
    public class DecimalPrecisionAttribute(int precision) : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value == null)
                return true;

            if (value is decimal price)
            {
                var decimalPlaces = BitConverter.GetBytes(decimal.GetBits(price)[3])[2];
                return decimalPlaces <= precision;
            }

            return false;
        }
    }
}
