﻿using Domain.Entities;

namespace Domain.Constants
{
    public static class Constants
    {
        public const string PRODUCT_CACHE_KEY = nameof(Product);
        public const string PRODUCTS_CACHE_KEY = "Products";
        public const string ORDER_CACHE_KEY = nameof(Order);
    }
}
