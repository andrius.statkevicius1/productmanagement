﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.DBContexts
{
    public class OrdersInfoContext(DbContextOptions<OrdersInfoContext> options) : DbContext(options)
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasIndex(e => e.UserId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
