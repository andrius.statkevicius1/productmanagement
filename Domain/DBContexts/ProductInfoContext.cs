﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.DBContexts
{
    public class ProductInfoContext(DbContextOptions<ProductInfoContext> options) : DbContext(options)
    {
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasIndex(e => e.Name)
                .IsUnique();
            base.OnModelCreating(modelBuilder);
        }
    }
}
