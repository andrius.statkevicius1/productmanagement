﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Domain.DBContexts
{
    public class UsersContext(DbContextOptions<UsersContext> options) : IdentityDbContext<IdentityUser>(options) { }
}
