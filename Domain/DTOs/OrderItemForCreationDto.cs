﻿using System.ComponentModel.DataAnnotations;

namespace Domain.DTOs
{
    public record OrderItemForCreationDto([Required] int? ProductId, [Required] int? Quantity);
}
