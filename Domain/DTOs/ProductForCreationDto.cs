﻿using Domain.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Domain.DTOs
{
    public record ProductForCreationDto(
        [Required(ErrorMessage = "You should provide a name value")] string Name,
        string Description,
        int StockQuantity,
        [Required(ErrorMessage = "You should provide a price value")]
        [DecimalPrecision(2, ErrorMessage = "Price must have maximum 2 digits precision")] decimal Price);
}
