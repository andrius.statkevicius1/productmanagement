﻿namespace Domain.DTOs
{
    public record class ProductForUpdateDto
    {
        public string? Name { get; init; }
        public string? Description { get; init; }
        public int? StockQuantity { get; init; }
        public decimal? Price { get; init; }
    }
}
