﻿using System.ComponentModel.DataAnnotations;

namespace Domain.DTOs
{
    public record ProductToAddToOrderDto([Required]int ProductId, [Required]int Quantity);
}
