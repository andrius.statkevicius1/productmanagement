﻿using System.ComponentModel.DataAnnotations;

namespace Domain.DTOs
{
    public record UserLoginRequestDto
    {
        [Required]
        public string? Email { get; init; }

        [Required]
        public string? Password { get; init; }
    }
}
