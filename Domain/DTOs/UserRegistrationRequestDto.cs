﻿using System.ComponentModel.DataAnnotations;

namespace Domain.DTOs
{
    public record UserRegistrationRequestDto
    {
        [Required]
        public string? Name { get; init; }

        [Required]
        public string? Email { get; init; }

        [Required]
        public string? Password { get; init; }
    }
}
