﻿namespace Domain.Entities
{
    public record AuthResult
    {
        public string? Token { get; init; }
        public bool Result { get; init; }
        public List<string>? Errors { get; init; }
    }
}
