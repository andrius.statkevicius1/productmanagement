﻿namespace Domain.Entities
{
    public class ProductsResult
    {
        public IEnumerable<Product>? Products { get; set; }
        public PaginationMetaData? PaginationMetaData { get; set; }
    }
}
