﻿using AutoMapper;
using Domain.DTOs;
using Domain.Entities;

namespace Domain.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductForUpdateDto, Product>();
            CreateMap<ProductForCreationDto, Product>();
        }
    }
}
