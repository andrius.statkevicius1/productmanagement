﻿using Domain.Entities;

namespace Model.Repositories
{
    public interface IProductInfoRepository
    {
        void DeleteProduct(Product productToDelete);
        Task<Product?> GetSingleProductAsync(int id, CancellationToken cancellationToken);
        Task<ProductsResult> ListAsync(string? name, string? searchQuery, int pageNumber, int pageSize, CancellationToken cancellationToken);
        Task RegisterProductAsync(Product product, CancellationToken cancellationToken);
        Task SaveChangesAsync(CancellationToken cancellationToken);
    }
}
