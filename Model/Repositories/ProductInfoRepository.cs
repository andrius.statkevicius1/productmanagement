﻿using Domain.DBContexts;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Model.Repositories
{
    public class ProductInfoRepository(ProductInfoContext context, ILogger<ProductInfoRepository> log) : IProductInfoRepository
    {
        public async Task<ProductsResult> ListAsync(string? name, string? searchQuery, int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            try
            {
                var collection = context.Products as IQueryable<Product>;

                if (!string.IsNullOrWhiteSpace(name))
                {
                    name = name?.Trim();
                    collection!.Where(x => x.Name == name);
                }

                if (!string.IsNullOrWhiteSpace(searchQuery))
                {
                    searchQuery = searchQuery.Trim();
                    collection = collection!.Where(x => x.Name!.Contains(searchQuery));
                }

                var totalItemCount = await collection!.CountAsync(cancellationToken: cancellationToken);
                var paginationMetaData = new PaginationMetaData(totalItemCount, pageSize, pageNumber);

                return new()
                {
                    Products = await collection!
                                .OrderBy(c => c.Name)
                                .Skip(pageSize * (pageNumber - 1))
                                .Take(pageSize)
                                .ToListAsync(cancellationToken: cancellationToken),
                    PaginationMetaData = paginationMetaData
                };
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Failed to fetch the products");
                return new();
            }
        }

        public async Task<Product?> GetSingleProductAsync(int id, CancellationToken cancellationToken) => await context.Products.SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
        public async Task RegisterProductAsync(Product product, CancellationToken cancellationToken) => await context.Products.AddAsync(product, cancellationToken);
        public async Task SaveChangesAsync(CancellationToken cancellationToken) => await context.SaveChangesAsync(cancellationToken);
        public void DeleteProduct(Product productToDelete) => context.Products.Remove(productToDelete);
    }
}
