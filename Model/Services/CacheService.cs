﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis;

namespace Model.Services
{
    public class CacheService(IDistributedCache distributedCache, IConfiguration configuration) : ICacheService
    {
        public async Task ClearCacheAsync(List<string> keys, CancellationToken cancellationToken)
        {
            var nonEmptyKeys = keys.Where(k => !string.IsNullOrEmpty(k));
            if (!nonEmptyKeys.Any())
                return;

            var tasks = nonEmptyKeys.Select(async key => await distributedCache.RemoveAsync(key));
            await Task.WhenAll(tasks);
        }

        public async Task ClearCacheContainingPatternAsync(string? pattern)
        {
            if (string.IsNullOrWhiteSpace(pattern))
                return;
            
            var redisConnection = await ConnectionMultiplexer.ConnectAsync(configuration.GetConnectionString("Redis")!);
            var server = redisConnection.GetServer(redisConnection.GetEndPoints().First());
            var keys = server.Keys(pattern: $"*{pattern}*");

            var tasks = keys.Where(k => !string.IsNullOrEmpty(k)).Select(async k => await distributedCache.RemoveAsync(k!));
            await Task.WhenAll(tasks);
        }

        public async Task SetCacheAsync<T>(string key, T cacheMember, TimeSpan timeSpan, CancellationToken cancellationToken) where T : class
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            await distributedCache.SetStringAsync(key, JsonConvert.SerializeObject(cacheMember, settings), new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = timeSpan }, cancellationToken);
        }

        public async Task<T?> TryGetCachedMemberAsync<T>(string key, CancellationToken cancellationToken) where T : class
        {
            var cachedString = await distributedCache.GetStringAsync(key, cancellationToken);
            if (!string.IsNullOrEmpty(cachedString))
                return JsonConvert.DeserializeObject<T>(cachedString);

            return null;
        }
    }
}
