﻿using Microsoft.Extensions.Caching.Distributed;

namespace Model.Services
{
    public interface ICacheService
    {
        Task SetCacheAsync<T>(string key, T cacheMember, TimeSpan timeSpan, CancellationToken cancellationToken) where T : class;
        Task<T?> TryGetCachedMemberAsync<T>(string key, CancellationToken cancellationToken) where T : class;
        Task ClearCacheAsync(List<string> keys, CancellationToken cancellationToken);
        Task ClearCacheContainingPatternAsync(string? pattern);
    }
}
