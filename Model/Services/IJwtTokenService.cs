﻿using Microsoft.AspNetCore.Identity;

namespace Model.Services
{
    public interface IJwtTokenService
    {
        string GenerateJwtToken(IdentityUser user);
    }
}