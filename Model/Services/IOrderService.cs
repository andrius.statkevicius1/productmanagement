﻿using Domain.DTOs;
using Domain.Entities;

namespace Model.Services
{
    public interface IOrderService
    {
        Task<Order?> GetSingleOrderAsync(int id, CancellationToken cancellationToken, bool includeChildItems = true);
        Task<IEnumerable<Order>> GetUserOrdersAsync(string? userId);
        Task<Order?> PlaceOrderAsync(string? userId, List<OrderItemForCreationDto>? orderItems, CancellationToken cancellationToken);
        Task<bool> TryUpdateOrderAsync(int orderId, List<OrderItemForCreationDto> productsToAdd, CancellationToken cancellationToken);
    }
}