﻿using Domain.Entities;

namespace Model.Services
{
    public interface IProductService
    {
        void DeleteProduct(Product productToDelete);
        Task<Product?> GetSingleProductAsync(int productId, CancellationToken cancellationToken);
        Task<ProductsResult> ListAsync(string? name, string? searchQuery, int pageNumber, int pageSize, CancellationToken cancellationToken);
        Task RegisterProductAsync(Product product, CancellationToken cancellationToken);
        Task SaveChangesAsync(int productId, CancellationToken cancellationToken, bool isUpdate = false);
    }
}