﻿using AutoMapper;
using Domain.Constants;
using Domain.DBContexts;
using Domain.DTOs;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Model.Services
{
    public class OrderService(
        OrdersInfoContext dbContext,
        IProductService productService,
        IMapper mapper,
        ICacheService cacheService
        ) : IOrderService
    {
        public async Task<Order?> GetSingleOrderAsync(int id, CancellationToken cancellationToken, bool includeChildItems = true)
        {
            var key = $"{Constants.ORDER_CACHE_KEY}_{id}";
            Order? order = await cacheService.TryGetCachedMemberAsync<Order>(key, cancellationToken);
            if (order is not null)
                return order;

            order = await dbContext!.Orders!.Include(o => o.OrderItems)!.SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
            if (order == null)
                return null;

            if (!includeChildItems)
                return order;

            foreach (var orderItem in order.OrderItems!)
            {
                var product = await productService.GetSingleProductAsync(orderItem.ProductId, cancellationToken);
                if (product is null)
                    continue;

                orderItem.Product = product;
            }

            await cacheService.SetCacheAsync(key, order, TimeSpan.FromHours(1), cancellationToken);
            return order;
        }

        public async Task<Order?> PlaceOrderAsync(string? userId, List<OrderItemForCreationDto>? orderItems, CancellationToken cancellationToken)
        {
            using var transaction = await dbContext.Database.BeginTransactionAsync(cancellationToken);
            var order = new Order { UserId = userId, CreatedAt = DateTime.UtcNow };
            await dbContext.Orders.AddAsync(order, cancellationToken);

            var groupedOrders = GroupOrderItems(orderItems);

            var orders = mapper.Map<List<OrderItem>>(groupedOrders);
            await AddOrderItemsAsync(order, orders, cancellationToken);

            await dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);
            return order;
        }

        private async Task AddOrderItemsAsync(Order order, List<OrderItem> orders, CancellationToken cancellationToken)
        {
            foreach (var item in orders)
            {
                var product = await productService.GetSingleProductAsync(item.ProductId, cancellationToken);
                if (product is null)
                    continue;

                item.OrderId = order.Id;
                item.TotalPrice = product.Price!.Value * item.Quantity;
                await dbContext.OrderItems.AddAsync(item, cancellationToken);
            }
        }

        private static IEnumerable<OrderItemForCreationDto>? GroupOrderItems(List<OrderItemForCreationDto>? orderItems) =>
            orderItems?
                .GroupBy(x => x.ProductId)
                .Select(group => new OrderItemForCreationDto(group.Key, group.Sum(x => x.Quantity)));

        public async Task<bool> TryUpdateOrderAsync(int orderId, List<OrderItemForCreationDto> productsToAdd, CancellationToken cancellationToken)
        {
            var existingOrder = await GetSingleOrderAsync(orderId, cancellationToken, includeChildItems: false);
            if (existingOrder is null)
                return false;

            using var transaction = await dbContext.Database.BeginTransactionAsync(cancellationToken);
            var groupedOrders = GroupOrderItems(productsToAdd);
            var orders = mapper.Map<List<OrderItem>>(groupedOrders);

            await AddOrderItemsAsync(existingOrder, orders, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            await cacheService.ClearCacheContainingPatternAsync($"{Constants.ORDER_CACHE_KEY}_{orderId}");
            return true;
        }
        
        public async Task<IEnumerable<Order>> GetUserOrdersAsync(string? userId)
        {
            var orders = dbContext.Orders as IQueryable<Order>;
            var result = orders.Where(x => x.UserId == userId).Include(x => x.OrderItems).OrderBy(x => x.Id);

            return await result.ToListAsync();
        }
    }
}