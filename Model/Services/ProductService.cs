﻿using Domain.Constants;
using Domain.Entities;
using Model.Repositories;

namespace Model.Services
{
    public class ProductService(IProductInfoRepository productInfoRepository, ICacheService cacheService) : IProductService
    {
        public void DeleteProduct(Product productToDelete) => productInfoRepository.DeleteProduct(productToDelete);
        public async Task<Product?> GetSingleProductAsync(int productId, CancellationToken cancellationToken)
        {
            var key = $"{Constants.PRODUCT_CACHE_KEY}-{productId}";
            Product? product = await cacheService.TryGetCachedMemberAsync<Product>(key, cancellationToken);
            if (product is null)
            {
                product = await productInfoRepository.GetSingleProductAsync(productId, cancellationToken);
                if (product is null)
                    return product;

                await cacheService.SetCacheAsync(key, product, TimeSpan.FromMinutes(5), cancellationToken);
                return product;
            }

            return product;
        }

        public async Task<ProductsResult> ListAsync(string? name, string? searchQuery, int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            var key = $"{Constants.PRODUCTS_CACHE_KEY}_{pageNumber}_{pageSize}";
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(searchQuery))
            {
                var cachedMember = await cacheService.TryGetCachedMemberAsync<ProductsResult>(key, cancellationToken);
                if (cachedMember is not null)
                    return cachedMember;
            }

            var result = await productInfoRepository.ListAsync(name, searchQuery, pageNumber, pageSize, cancellationToken);
            await cacheService.SetCacheAsync(key, result, TimeSpan.FromHours(1), cancellationToken);
            return result;
        }

        public async Task RegisterProductAsync(Product product, CancellationToken cancellationToken)
        {
            await productInfoRepository.RegisterProductAsync(product, cancellationToken);
            await cacheService.ClearCacheContainingPatternAsync(Constants.PRODUCTS_CACHE_KEY);
        }

        public async Task SaveChangesAsync(int productId, CancellationToken cancellationToken, bool isUpdate = false)
        {
            if (isUpdate)
                await cacheService.ClearCacheAsync([$"{Constants.PRODUCT_CACHE_KEY}-{productId}"], cancellationToken);

            await cacheService.ClearCacheContainingPatternAsync(Constants.ORDER_CACHE_KEY);
            await productInfoRepository.SaveChangesAsync(cancellationToken);
        }
    }
}
