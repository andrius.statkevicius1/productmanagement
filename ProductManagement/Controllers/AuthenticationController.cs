﻿using Domain.DTOs;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Model.Services;

namespace ProductManagement.API.Controllers
{
    /// <summary>
    /// Controller for managing users.
    /// </summary>
    [Produces("application/json")]
    [Route("api/Authentication")]
    [ApiController]
    public class AuthenticationController(
        UserManager<IdentityUser> userManager,
        IJwtTokenService jwtTokenService
        ) : ControllerBase
    {
        /// <summary>
        /// Registers a new user
        /// </summary>
        /// <param name="requestDto">Request's payload</param>
        [HttpPost("Register")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<IActionResult> RegisterAsync(UserRegistrationRequestDto requestDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var existingUser = await userManager.FindByEmailAsync(requestDto.Email!);
            if (existingUser is not null)
                return BadRequest(new AuthResult() { Result = false, Errors = new() { { "Email already exists" } } });

            var newUser = new IdentityUser { Email = requestDto.Email, UserName = requestDto.Email };
            var creationResult = await userManager.CreateAsync(newUser, requestDto.Password!);
            if (creationResult.Succeeded)
            {
                var token = jwtTokenService.GenerateJwtToken(newUser);
                return Ok(new AuthResult() { Result = true, Token = token });
            }

            return BadRequest(new AuthResult() { Result = false, Errors = new() { { "Server error" } } });
        }

        /// <summary>
        /// Logs in a new user
        /// </summary>
        /// <param name="loginRequest">Request's payload</param>
        [HttpPost("Login")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<IActionResult> LoginAsync(UserLoginRequestDto loginRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var existingUser = await userManager.FindByEmailAsync(loginRequest.Email!);
            if (existingUser is null)
                return BadRequest(new AuthResult() { Errors = ["User does not exist"], Result = false });

            var isCorrect = await userManager.CheckPasswordAsync(existingUser, loginRequest.Password!);
            if (!isCorrect)
                return BadRequest(new AuthResult() { Errors = ["Invalid credentials"], Result = false });

            var jwtToken = jwtTokenService.GenerateJwtToken(existingUser);
            return Ok(new AuthResult() { Token = jwtToken, Result = true });
        }
    }
}
