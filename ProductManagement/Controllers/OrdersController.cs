﻿using AutoMapper.Configuration.Annotations;
using Domain.DTOs;
using Domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Services;
using System.IdentityModel.Tokens.Jwt;

namespace ProductManagement.API.Controllers
{
    /// <summary>
    /// Controller for managing product orders.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/orders")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrdersController(IOrderService orderService, ILogger<OrdersController> log) : ControllerBase
    {
        /// <summary>
        /// Places an order for the current user.
        /// </summary>
        /// <param name="id">Id of the order to get</param>
        [HttpGet("{id:int}", Name = nameof(GetOrderAsync))]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<Order>> GetOrderAsync(int id, CancellationToken cancellationToken)
        {
            Order? orderToReturn = await orderService.GetSingleOrderAsync(id, cancellationToken);
            return orderToReturn is null ? NotFound($"An order with id: {id} has not been found") : Ok(orderToReturn);
        }

        /// <summary>
        /// Places an order for the current user.
        /// </summary>
        /// <param name="orderItems">Request's payload</param>
        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<ActionResult<Order>> PlaceOrderAsync(List<OrderItemForCreationDto> orderItems, CancellationToken cancellationToken)
        {
            try
            {
                if (TryGetUserId(out string? userId))
                {
                    var order = await orderService.PlaceOrderAsync(userId, orderItems, cancellationToken);

                    return order is not null
                        ? CreatedAtRoute(nameof(GetOrderAsync), new { id = order.Id }, order)
                        : BadRequest("An order has not been created due to errors in it");
                }
                else
                {
                    return BadRequest("Authorization header missing");
                }
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured while placing order");
                return BadRequest("Unknown error");
            }
        }

        private bool TryGetUserId(out string? userId)
        {
            userId = string.Empty;
            var authorizationHeader = HttpContext.Request.Headers.Authorization.ToString();
            var token = authorizationHeader["Bearer ".Length..].Trim();
            if (string.IsNullOrWhiteSpace(token))
                return false;

            if (new JwtSecurityTokenHandler().ReadToken(token) is not JwtSecurityToken jsonToken)
                return false;

            userId = jsonToken.Claims.FirstOrDefault(claim => claim.Type == "Id")?.Value;
            return !string.IsNullOrWhiteSpace(userId);
        }

        /// <summary>
        /// Adds products to the existing order.
        /// </summary>
        /// <param name="orderId">Id of an existing order</param>
        /// <param name="productsToAdd">Request's body</param>
        [HttpPut("{orderId:int}/add-products")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> AddProductsToOrder(int orderId, [FromBody] List<OrderItemForCreationDto> productsToAdd, CancellationToken cancellationToken)
        {
            try
            {
                if (TryGetUserId(out var userId))
                {
                    var order = await orderService.GetSingleOrderAsync(orderId, cancellationToken, includeChildItems: false);
                    if (order == null)
                        return BadRequest($"Order by provided id: {orderId} not found");
                    else if (order.UserId != userId)
                        return BadRequest("Requested order does not belong to the user");
                }

                if (!await orderService.TryUpdateOrderAsync(orderId, productsToAdd, cancellationToken))
                    return BadRequest($"Order by provided id: {orderId} not found");

                return NoContent();
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured while updating order");
                return BadRequest("Unknown error");
            }
        }

        /// <summary>
        /// Gets all orders for the current user
        /// </summary>
        [HttpGet]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrdersAsync()
        {
            try
            {
                if (TryGetUserId(out var userId))
                {
                    var result = await orderService.GetUserOrdersAsync(userId);
                    return Ok(result);
                }
                else
                {
                    return Ok(Enumerable.Empty<Order>());
                }
            }
            catch (Exception ex)
            {
                log.LogError(ex, "An error occured while getting user's orders");
                return BadRequest("Unknown error");
            }
        }
    }
}
