﻿using AutoMapper;
using Domain.DTOs;
using Domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Services;
using System.Text.Json;

namespace ProductManagement.API.Controllers
{
    /// <summary>
    /// Controller for managing products.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/products")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController(IProductService productService, IMapper mapper) : ControllerBase
    {
        private const int MAX_STOCKS_PAGE_SIZE = 20;

        /// <summary>
        /// Retrieves a list of products.
        /// </summary>
        /// <returns>A list of products.</returns>
        /// <param name="name">Name of the product to search by</param>
        /// <param name="searchQuery">Part of the product name used as search query</param>
        /// <param name="pageNumber">Page number used for paginating products</param>
        /// <param name="pageSize">Page size used for paginating products</param>
        [HttpGet]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<IEnumerable<Product>>> GetProductsAsync([FromQuery] string? name,
                                                                              [FromQuery] string? searchQuery,
                                                                              CancellationToken cancellationToken,
                                                                              [FromQuery] int pageNumber = 1,
                                                                              [FromQuery] int pageSize = 10)
        {
            pageSize = pageSize > MAX_STOCKS_PAGE_SIZE ? MAX_STOCKS_PAGE_SIZE : pageSize;

            var result = await productService.ListAsync(name, searchQuery, pageNumber, pageSize, cancellationToken);
            Response.Headers.Append("X-Pagination", JsonSerializer.Serialize(result.PaginationMetaData));
            return Ok(result.Products);
        }

        /// <summary>
        /// Retrieves a product by id.
        /// </summary>
        /// <returns>A product by id</returns>
        /// <param name="id">Product id to search by</param>
        [HttpGet("{id:int}", Name = nameof(GetProduct))]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Get))]
        public async Task<ActionResult<Product>> GetProduct(int id, CancellationToken cancellationToken)
        {
            var productToReturn = await productService.GetSingleProductAsync(id, cancellationToken);
            if (productToReturn is null)
                return NotFound();

            return Ok(productToReturn);
        }

        /// <summary>
        /// Registers a new product
        /// </summary>
        /// <param name="productToRegister">Request's payload</param>
        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Post))]
        public async Task<ActionResult<Product>> RegisterProduct(ProductForCreationDto productToRegister, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var product = mapper.Map<Product>(productToRegister);
            await productService.RegisterProductAsync(product, cancellationToken);
            await productService.SaveChangesAsync(product.Id, cancellationToken);

            return CreatedAtRoute(nameof(GetProduct), new { id = product.Id }, product);
        }

        /// <summary>
        /// Updates an existing product if present by id
        /// </summary>
        /// <param name="productForUpdate">Request's payload</param>
        /// <param name="id">id of the product</param>
        [HttpPut("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Put))]
        public async Task<ActionResult> UpdateProductAsync(int id, ProductForUpdateDto productForUpdate, CancellationToken cancellationToken)
        {
            var product = await productService.GetSingleProductAsync(id, cancellationToken);
            if (product is null)
                return NotFound();

            mapper.Map(productForUpdate, product);
            await productService.SaveChangesAsync(product.Id, cancellationToken, true);
            return NoContent();
        }

        /// <summary>
        /// Deletes an existing product if present by id
        /// </summary>
        /// <param name="id">Id of the product</param>
        [HttpDelete("{id:int}")]
        [ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Delete))]
        public async Task<ActionResult> DeleteProduct(int id, CancellationToken cancellationToken)
        {
            var productToDelete = await productService.GetSingleProductAsync(id, cancellationToken);
            if (productToDelete is null)
                return NotFound();

            productService.DeleteProduct(productToDelete);
            await productService.SaveChangesAsync(productToDelete.Id, cancellationToken);
            return NoContent();
        }
    }
}
