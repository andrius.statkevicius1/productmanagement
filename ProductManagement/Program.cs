using Domain.Configurations;
using Domain.DBContexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Model.Repositories;
using Model.Services;
using System.Reflection;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Products Management API",
        Description = "API for retrieving/managing products and orders"
    });

    s.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Scheme = "bearer"
    });

    s.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "bearer"
                }
            },
            new List<string>()
        }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    s.IncludeXmlComments(xmlPath);
});

builder.Services.AddDbContext<ProductInfoContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("ProductInfoDBConnectionString")));
builder.Services.AddDbContext<OrdersInfoContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("OrdersInfoDBConnectionString")));
builder.Services.AddDbContext<UsersContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("UsersInfoDBConnectionString")));
builder.Services.AddDefaultIdentity<IdentityUser>(options =>
{
    options.SignIn.RequireConfirmedAccount = false;
})
.AddEntityFrameworkStores<UsersContext>();

builder.Services.Configure<JwtConfig>(builder.Configuration.GetSection(nameof(JwtConfig)));

var isDevelopment = builder.Environment.IsDevelopment();
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = options.DefaultScheme = options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

}).AddJwtBearer(jwt =>
{
    var key = Encoding.ASCII.GetBytes(builder.Configuration.GetSection($"{nameof(JwtConfig)}:Secret").Value!);
    jwt.SaveToken = true;
    jwt.TokenValidationParameters = new()
    {
        ValidateIssuer = !isDevelopment,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateAudience = !isDevelopment,
        RequireExpirationTime = !isDevelopment, // needs to be updated when refresh token is implemented
        ValidateLifetime = true
    };
});

builder.Services.AddStackExchangeRedisCache(redisOptions =>
{
    var connection = builder.Configuration.GetConnectionString("Redis"); 
    redisOptions.Configuration = connection;
});
builder.Services.AddScoped<IProductInfoRepository, ProductInfoRepository>();
builder.Services.AddScoped<IJwtTokenService, JwtTokenService>();
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<ICacheService, CacheService>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
